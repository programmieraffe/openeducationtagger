// 'use strict'

// http://bootboxjs.com/getting-started.html
import bootbox from 'bootbox'

import OpenEducationTagger from './openeducationtaggerclass.js';


// console.log fallback
if (typeof console === 'undefined') {
  window.console = {
    log: function() {},
    error: function() {}
  };
}

// 'use strict'

// this approach is needed because nodeIntegration = false

// selected node APIs are exposed here (e.g. https), but not all
// https://github.com/electron/electron/issues/8227

//document.onreadystatechange = function() {
//if (document.readyState == "complete") {
// 2DO: load via npm?
// const $ = require('jquery');
// Do things with $

// loaded via index.html
$(function() {
  console.log('hello jquery world');

  // 2DO: is this secure?
  //window.https = require('https');
  //window.tryjson = require('tryjson');

  const configStore = localStorage;

  console.log('try to create class instance');
  const myOpenEducationTagger = new OpenEducationTagger();

  // these will be saved to local storage automatically
  const configFormFields = [
    'oet_spreadsheet_id',
    'oet_spreadsheet_sheet_id',
    'oet_elasticsearch_hostname',
    'oet_elasticsearch_index',
    'oet_elasticsearch_auth_string_write',
    'oet_elasticsearch_auth_string_read'
  ];

  var updateSearchUiUrl = function() {
    console.log('updateSearchUiUrl called, config store:', configStore);

    // 2DO: use vars, just quick & dirty here
    // 2DO: ugly, solve better :(
    if (configStore.getItem("oet_elasticsearch_hostname") != null && configStore.getItem("oet_elasticsearch_hostname") != "" && configStore.getItem("oet_elasticsearch_index") != null && configStore.getItem("oet_elasticsearch_index") != "" && configStore.getItem("oet_elasticsearch_auth_string_read") != null && configStore.getItem("oet_elasticsearch_auth_string_read") != "") {
      // example: https://programmieraffe.gitlab.io/openeducationtagger-search/?url=https://coronacampus-691643726.eu-central-1.bonsaisearch.net&index=index1&auth=qjoZXnydFC:gPY6fNtC5VTDBbXahckQ
      const newUrl = 'https://programmieraffe.gitlab.io/openeducationtagger-search/?url=https://' + configStore.getItem("oet_elasticsearch_hostname") + '&auth=' + configStore.getItem("oet_elasticsearch_auth_string_read") + '&index=' + configStore.getItem("oet_elasticsearch_index");

      $("#searchUiUrl").val(newUrl);
      $("#oetOpenSearchUi").prop('href', newUrl);
    } else {
      $("#searchUiUrl").val("Please fill in configuration values (hostname,index, auth read-only)");
      $("#oetOpenSearchUi").prop('href', '');
    }


  }

  // we do not store oet_elasticsearch_auth_string_write, unless user explictly wants this
  // just checkbox things
  // if it is in store, user had checkbox enabled, so we enable it again
  if ("oet_elasticsearch_auth_string_write" in localStorage) {
    $("#storeAuthStringWriteInLocalStorage").prop('checked', true);
  }
  $("#storeAuthStringWriteInLocalStorage").on('change', function() {
    // retrigger possible save event for field after checkbox was activated/deactived
    // this will save form field in config store if checked
    $("input[name='oet_elasticsearch_auth_string_write']").trigger('change');
  });

  // forEach -> launches own callback, not for:i action
  configFormFields.forEach(function(formFieldName) {
    // 2DO: load values

    const valueInStore = configStore.getItem(formFieldName);
    console.log('check if value is in store', formFieldName, " = ", valueInStore);

    // if value is already stored, apply it to form field
    if (valueInStore !== null) {
      // 2DO: use ids/better selectors
      //console.log('set input form to stored value');
      $("input[name=" + formFieldName + "]").val(valueInStore);

      // special function, trigger the URL generation for search UI
      if (formFieldName == 'oet_elasticsearch_auth_string_read') {
        updateSearchUiUrl();
      }
    }

    // initiate jquery event
    $("input[name=" + formFieldName + "]").bind('change', function() {
      //$("input[name=" + formFieldName + "]").bind('input propertychange', function() {
      console.log('event - form field value was changed', this, this.name);
      if (this.name !== 'oet_elasticsearch_auth_string_write' || (this.name == 'oet_elasticsearch_auth_string_write' && $("#storeAuthStringWriteInLocalStorage").is(":checked"))) {
        console.log('save new value to config store', this.value);
        configStore.setItem(this.name, this.value);
      }
    });

    // special: set predefined value for sheet_id if empty
    /*if(formFieldName == 'oet_spreadsheet_sheet_id' && valueInStore == null){
      $("input[name=" + formFieldName + "]").val('od6');
    }*/

  }, this); // 2DO: is bind needed?



  $("form").on('change', function() {
    console.log('form change event triggered');
    // call update function
    // 2DO: find better position for that
    updateSearchUiUrl();
  });

  $("#oetSyncSpreadsheet").on('click', function(e) {
    e.preventDefault();

    console.log('############## START SYNC #############');
    console.log('Try to sync spreadsheet ...');
    console.log('Preparing config ...')

    const currentConfig = {};
    for (let i = 0; i < configFormFields.length; i++) {

      const itemVal = $("input[name=" + configFormFields[i] + "]:first").val();
      console.log('Getting config value for '+configFormFields[i]+'', itemVal);
      currentConfig['' + configFormFields[i] + ''] = itemVal;

      // simple validation, only read key is not required for sync
      if (configFormFields[i] !== 'oet_elasticsearch_auth_string_read') {
        // validate value not empty
        if (itemVal == '' || itemVal == null || itemVal == undefined) {
          bootbox.alert({
            title: "Error",
            message: "No value for " + configFormFields[i] + " submitted, please enter one"
          });
          // break execution
          console.log('Validation error, stopping sync...');
          return false;
        }
      }
    }
    console.log('Config for sync is set ...', currentConfig);
    console.log('Submit config values to opeducationtaggerclass.js ...');
    myOpenEducationTagger.setConfig(currentConfig);
    myOpenEducationTagger.startSync();
    bootbox.alert({
      title: "Process started ...",
      message: "The insert process was started, please check out Firefox/Chrome developer tools console to see the detailed progress."
    });

  }); // eo click

  // button actions
  $(".btn-oetDeleteLocalStorage").on('click', function(e) {
    // 2DO: reload form/window?
    e.preventDefault();
    $("input").val('');
    localStorage.clear();
    $("#storeAuthStringWriteInLocalStorage").prop('checked', false);
    console.log('Local storage should be deleted by now', localStorage);
  });

  $("#oetTestBulk").on('click', function() {
    myOpenEducationTagger.setConfig(configStore);
    myOpenEducationTagger.testBulkApi();
  });

  $("#oetDeleteBySpreadsheetId").on('click', function() {
    myOpenEducationTagger.setConfig(configStore);
    myOpenEducationTagger.deleteBySpreadsheetId();
  });

  //$('#oetSyncSpreadsheet').focus(); // focus input box

  $("#oetDismissNavbarNotice").on('click',function(e){
    e.preventDefault();
    $("#navbarTop").hide();
  });

  // generate searchUiUrl

  // https://programmieraffe.gitlab.io/openeducationtagger-frontend-search/?url=scalr.api.appbase.io&index=openeducationtagger-playground&auth=LtQO5QbO1%3A1ea913da-ce52-460d-9625-9a5f0f41af56

}); // eo jQuery

//} // eo readyState == complete
//} // eo onReadstayte
