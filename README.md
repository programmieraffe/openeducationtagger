# OpenEducationTagger 👩‍💻🔎

**Build your own little educational search engine**


## Open web tool (v.0.9):

👉 **[programmieraffe.gitlab.io/openeducationtagger/](https://programmieraffe.gitlab.io/openeducationtagger/)**

⚠️ Work in progress. Early alpha, no professional product / Documentation will be updated in the next days ⚠️

## Description

Collect freely accessible teaching/learning resource together and build your own little search engine with a nice user interface - sounds great? The best thing: no nerdy command line usage necessary.

📝 Collect educational resources in a web spreadsheet<br>
🗂 Setup an elasticsearch index<br>
➡️ Send spreadsheet data to elasticsearch<br>
🔎 Provide nice-looking search interface

### Architecture

![oet infrastructure](infrastructure.png)

Image source: [https://kurzelinks.de/ubfb](https://kurzelinks.de/ubfb)

### Inspired by

- [https://timeline.knightlab.com/](https://timeline.knightlab.com/)
- [https://dejavu.appbase.io/](https://dejavu.appbase.io/)

### Built with

- reactive-search (Open Source)
- Gitlab.com CI
- Logo/Favicon: Emoji designed by [OpenMoji](https://openmoji.org/) – the open-source emoji and icon project. License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/#)
- Tutorials and open source source codes provided by people from all over the world <3

### Helpful tools

- [https://dejavu.appbase.io/](https://dejavu.appbase.io/)

## See also:

- 👉 current project from german hackathon: ["DigiCampus"](https://twitter.com/FloRa_Education/status/1242056840671879168)
- 👉 Steffen Rörtgen [oerhoernchen20-docker](https://github.com/sroertgen/oerhoernchen20_docker/tree/master/docker_hoernchen)
- 👉 professional: [https://gitlab.com/oersi](gitlab.com/oersi), [SkoHub](https://skohub.io/) [x5gon](https://www.x5gon.org/), [oasis.geneseo.edu/](https://oasis.geneseo.edu/)
- 👉 upcoming: Social network approach with federated instances  [moodle.net/](https://moodle.net/) 💛

# Deveveloper notes

## Open it

https://programmieraffe.gitlab.io/openeducationtagger/

* See infos & full documentation: https://github.com/programmieraffe/openeducationtagger *

## Test / develop locally

1. `npm install`
2. `npm run start`

### Related repositories

- [gitlab.com/programmieraffe/openeducationtagger](https://gitlab.com/programmieraffe/openeducationtagger)
- [gitlab.com/programmieraffe/openeducationtagger-search](https://gitlab.com/programmieraffe/openeducationtagger-search)
- Advanced: [gitlab.com/programmieraffe/openeducationtagger-herokuworker](https://gitlab.com/programmieraffe/openeducationtagger-herokuworker)

## 2DO:

- add icons (images should be in public? but this was changed due to gitlab-runner for gitlab pages? can't quite figure out the webpack starterkit config with gitlab)
- weird bug: When requesting the google spreadsheet JSON feed.updated.$t is updated on every request, so we can't really change if the doc has changed... (maybe loggedin issue? )

## Built with - thanks!

- https://github.com/cichy380/html-starter-bs4-webpack/
- Gitlab CI example with webpack by https://gitlab.com/dan.stuart/gitlab-pages-example---webpack/-/tree/master

Logo/Favicon: Emoji designed by [OpenMoji](https://openmoji.org/) – the open-source emoji and icon project. License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/#)
